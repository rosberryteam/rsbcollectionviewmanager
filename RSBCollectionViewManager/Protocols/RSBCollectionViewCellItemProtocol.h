//Copyright (c) 2015 RSBCollectionViewManager
//
//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in
//all copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//THE SOFTWARE.

#import <UIKit/UIKit.h>

/*!
 * @discussion Every cell item should adopt this protocol for correct working with RSBCollectionViewManager.
 */
@protocol RSBCollectionViewCellItemProtocol <NSObject>

///Return size for cell class, managed by cell item.
- (CGSize)sizeForCollectionView:(UICollectionView *)collectionView
                         layout:(UICollectionViewLayout*)collectionViewLayout
                      indexPath:(NSIndexPath *)indexPath;

/*!
 * @discussion Use this method for configuring cell.
 * @warning Don't forget to call [super configureCell:atIndexPath:inCollectionView:] for right inheritance.
 */
- (void)configureCell:(UICollectionViewCell *)cell atIndexPath:(NSIndexPath*)indexPath inCollectionView:(UICollectionView *)collectionView;

@optional

/*!
 * @discussion If your cell should be loaded from xib provide UINib instance.
 * @warning You have to provide at least one of the following methods
 * registeredCollectionViewCellNib, registeredCollectionViewCellClass or 
 * storyboardPrototypeCollectionViewCellReuseIdentifier.
 */
- (UINib *)registeredCollectionViewCellNib;
/*!
 * @discussion If your cell should be created without xib provide it class.
 * @warning You have to provide at least one of the following methods
 * registeredCollectionViewCellNib, registeredCollectionViewCellClass or
 * storyboardPrototypeCollectionViewCellReuseIdentifier.
 */
- (Class)registeredCollectionViewCellClass;
/*!
 * @discussion If your cell should be loaded from storyboard return reuse identifier you
 * provide in storyboard.
 * @warning You have to provide at least one of the following methods
 * registeredCollectionViewCellNib, registeredCollectionViewCellClass or
 * storyboardPrototypeCollectionViewCellReuseIdentifier.
 */
- (NSString *)storyboardPrototypeCollectionViewCellReuseIdentifier;


///Cell did select in collection view at index path.
- (void)itemDidSelectInCollectionView:(UICollectionView *)collectionView atIndexPath:(NSIndexPath *)indexPath;

///Cell did deselect in collection view at index path.
- (void)itemDidDeselectInCollectionView:(UICollectionView *)collectionView atIndexPath:(NSIndexPath *)indexPath;

///Determines if cell item should be selected in collection view at index path.
- (BOOL)itemShouldSelectInCollectionView:(UICollectionView *)collectionView atIndexPath:(NSIndexPath *)indexPath;

///Determines if cell item should be highlighted in collection view at index path.
- (BOOL)itemShouldHighlightInCollectionView:(UICollectionView *)collectionView atIndexPath:(NSIndexPath *)indexPath;

///This method called when cell is about to display.
- (void)willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath inCollectionView:(UICollectionView *)collectionView;

///This method called when cell was removed from the collection view.
- (void)didEndDisplayingCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath inCollectionView:(UICollectionView *)collectionView;

@end
