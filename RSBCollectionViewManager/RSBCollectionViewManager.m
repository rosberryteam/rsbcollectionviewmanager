//Copyright (c) 2015 RSBCollectionViewManager
//
//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in
//all copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//THE SOFTWARE.

#import "RSBCollectionViewManager.h"
#import "RSBCollectionViewSectionItemProtocol.h"
#import "RSBCollectionViewCellItemProtocol.h"
#import "RSBCollectionViewReusableViewItemProtocol.h"

@interface RSBCollectionViewManager () <UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>

@end

@implementation RSBCollectionViewManager

- (instancetype)init {
    self = [self initWithCollectionView:[[UICollectionView alloc] init]];
    if (self) {
        
    }
    return self;
}

- (instancetype)initWithCollectionView:(UICollectionView *)collectionView {
    self = [super init];
    if (self) {
        _collectionView = collectionView;
        self.collectionView.delegate = self;
        self.collectionView.dataSource = self;
    }
    return self;
}

#pragma mark - Public methods

#pragma mark Cell items

- (void)removeCellItems:(NSArray <id<RSBCollectionViewCellItemProtocol>> *)cellItems
        fromSectionItem:(id<RSBCollectionViewSectionItemProtocol>)sectionItem
             completion:(nullable RSBCollectionViewManagerCompetionBlock)completion {
    
    NSUInteger section = [self.sectionItems indexOfObject:sectionItem];
    NSMutableArray *indexPaths = [NSMutableArray array];
    NSMutableIndexSet *indexes = [NSMutableIndexSet indexSet];
    
    for (id<RSBCollectionViewCellItemProtocol> cellItem in cellItems) {
        NSAssert([sectionItem.cellItems containsObject:cellItem], @"It's impossible to remove cell items that are not contained in section item %@", sectionItem);
        NSUInteger row = [sectionItem.cellItems indexOfObject:cellItem];
        [indexPaths addObject:[NSIndexPath indexPathForRow:row inSection:section]];
        [indexes addIndex:row];
    }
    
    NSMutableArray *mCellItems = [NSMutableArray arrayWithArray:sectionItem.cellItems];
    [mCellItems removeObjectsAtIndexes:indexes];
    sectionItem.cellItems = [NSArray arrayWithArray:mCellItems];
    
    [self.collectionView performBatchUpdates:^{
        [self.collectionView deleteItemsAtIndexPaths:indexPaths];
    } completion:^(BOOL finished) {
        if (completion) {
            completion(finished);
        }
    }];
}

- (void)insertCellItems:(NSArray <id<RSBCollectionViewCellItemProtocol>> *)cellItems
          toSectionItem:(id<RSBCollectionViewSectionItemProtocol>)sectionItem
              atIndexes:(NSIndexSet *)indexes
             completion:(nullable RSBCollectionViewManagerCompetionBlock)completion {
    
    NSAssert([indexes firstIndex] <= sectionItem.cellItems.count, @"It's impossible to insert item at index that is larger than count of cell items in this section");
    [cellItems enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        id<RSBCollectionViewCellItemProtocol> cellItem = obj;
        [self registerCellItem:cellItem];
    }];
    
    NSMutableArray *mCellItems = [NSMutableArray arrayWithArray:sectionItem.cellItems];
    [mCellItems insertObjects:cellItems atIndexes:indexes];
    sectionItem.cellItems = [NSArray arrayWithArray:mCellItems];
    
    NSUInteger section = [self.sectionItems indexOfObject:sectionItem];
    NSMutableArray *indexPaths = [NSMutableArray array];
    [indexes enumerateIndexesUsingBlock:^(NSUInteger idx, BOOL * _Nonnull stop) {
        [indexPaths addObject:[NSIndexPath indexPathForRow:idx inSection:section]];
    }];
    
    [self.collectionView performBatchUpdates:^{
        [self.collectionView insertItemsAtIndexPaths:indexPaths];
    } completion:^(BOOL finished) {
        if (completion) {
            completion(finished);
        }
    }];
}

- (void)replaceCellItemsAtIndexes:(NSIndexSet *)indexes
                    withCellItems:(NSArray<id<RSBCollectionViewCellItemProtocol>> *)cellItems
                    inSectionItem:(id<RSBCollectionViewSectionItemProtocol>)sectionItem
                       completion:(nullable RSBCollectionViewManagerCompetionBlock)completion {
    
    NSAssert(indexes.count == cellItems.count, @"It's impossible to replace not equals count of cell items");
    [cellItems enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        id<RSBCollectionViewCellItemProtocol> cellItem = obj;
        [self registerCellItem:cellItem];
    }];
    
    NSMutableArray *mutableItems = [NSMutableArray arrayWithArray:sectionItem.cellItems];
    [mutableItems replaceObjectsAtIndexes:indexes withObjects:cellItems];
    sectionItem.cellItems = [NSArray arrayWithArray:mutableItems];
    
    NSUInteger section = [self.sectionItems indexOfObject:sectionItem];
    NSMutableArray *indexPaths = [NSMutableArray array];
    [indexes enumerateIndexesUsingBlock:^(NSUInteger idx, BOOL * _Nonnull stop) {
        [indexPaths addObject:[NSIndexPath indexPathForRow:idx inSection:section]];
    }];
    
    [self.collectionView performBatchUpdates:^{
        [self.collectionView reloadItemsAtIndexPaths:indexPaths];
    } completion:^(BOOL finished) {
        if (completion) {
            completion(finished);
        }
    }];
}

#pragma mark Section items

- (void)removeSectionItems:(NSArray<id<RSBCollectionViewSectionItemProtocol>> *)sectionItems
                completion:(nullable RSBCollectionViewManagerCompetionBlock)completion {
    
    NSMutableIndexSet *indexes = [NSMutableIndexSet indexSet];
    for (id<RSBCollectionViewSectionItemProtocol> sectionItem in sectionItems) {
        NSAssert([self.sectionItems containsObject:sectionItem], @"It's impossible to remove section items that are not contained in section items array");
        NSUInteger section = [self.sectionItems indexOfObject:sectionItem];
        [indexes addIndex:section];
    }
    
    NSMutableArray *mItems = [NSMutableArray arrayWithArray:self.sectionItems];
    [mItems removeObjectsAtIndexes:indexes];
    
    [self.collectionView performBatchUpdates:^{
        self.sectionItems = [NSArray arrayWithArray:mItems];
        [self.collectionView deleteSections:indexes];
    } completion:^(BOOL finished) {
        if (completion) {
            completion(finished);
        }
    }];
}

- (void)insertSectionItems:(NSArray<id<RSBCollectionViewSectionItemProtocol>> *)sectionItems
                 atIndexes:(NSIndexSet *)indexes
                completion:(nullable RSBCollectionViewManagerCompetionBlock)completion {
    
    NSAssert([indexes firstIndex] <= self.sectionItems.count, @"It's impossible to insert item at index that is larger than count of section items");
    [sectionItems enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        id<RSBCollectionViewSectionItemProtocol> sectionItem = obj;
        [self registerSectionItem:sectionItem];
    }];
    
    NSMutableArray *mItems = [NSMutableArray arrayWithArray:self.sectionItems];
    [mItems insertObjects:sectionItems atIndexes:indexes];
    
    [self.collectionView performBatchUpdates:^{
        self.sectionItems = [NSArray arrayWithArray:mItems];
        [self.collectionView insertSections:indexes];
    } completion:^(BOOL finished) {
        if (completion) {
            completion(finished);
        }
    }];
}

- (void)replaceSectionItemsAtIndexes:(NSIndexSet *)indexes
                    withSectionItems:(NSArray<id<RSBCollectionViewSectionItemProtocol>> *)array
                          completion:(nullable RSBCollectionViewManagerCompetionBlock)completion {
    
    NSAssert(indexes.count == array.count, @"It's impossible to replace not equals count of section items");
    [array enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        [self registerSectionItem:obj];
    }];
    
    NSMutableArray *mutableSections = [NSMutableArray arrayWithArray:self.sectionItems];
    [mutableSections replaceObjectsAtIndexes:indexes withObjects:array];
    
    [self.collectionView performBatchUpdates:^{
        self.sectionItems = [NSArray arrayWithArray:mutableSections];
        [self.collectionView reloadSections:indexes];
    } completion:completion];
}

#pragma mark Others

- (void)setSectionItems:(NSArray *)sectionItems{
    [self setSectionItems:sectionItems completion:nil];
}

- (void)setSectionItems:(NSArray *)sectionItems completion:(void(^)(BOOL finished))completion{
    _sectionItems = sectionItems;
    [_sectionItems enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        [self registerSectionItem:obj];
    }];
    if (completion){
        [self.collectionView performBatchUpdates:^{
            [self.collectionView reloadSections:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0, _sectionItems.count)]];
        } completion:^(BOOL finished) {
            completion(finished);
        }];
    }
    else{
        [self.collectionView reloadData];
    }
}

- (CGRect)frameForCellItem:(id<RSBCollectionViewCellItemProtocol>)cellItem inSectionItem:(id<RSBCollectionViewSectionItemProtocol>)sectionItem {
    NSInteger sectionIndex = [self.sectionItems indexOfObject:sectionItem];
    if (sectionIndex < 0 || sectionIndex == NSNotFound) {
        return CGRectZero;
    }
    
    NSInteger cellIndex = [sectionItem.cellItems indexOfObject:cellItem];
    if (cellIndex < 0 || cellIndex == NSNotFound) {
        return CGRectZero;
    }
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:cellIndex inSection:sectionIndex];
    UICollectionViewLayoutAttributes *attributes = [self.collectionView layoutAttributesForItemAtIndexPath:indexPath];
    return attributes.frame;
}

- (void)scrollToCellItem:(id<RSBCollectionViewCellItemProtocol>)cellItem
           inSectionItem:(id<RSBCollectionViewSectionItemProtocol>)sectionItem
        atScrollPosition:(UICollectionViewScrollPosition)position
                animated:(BOOL)animated {
    
    NSInteger sectionItemIndex = [self.sectionItems indexOfObject:sectionItem];
    NSInteger cellItemIndex = [sectionItem.cellItems indexOfObject:cellItem];
    if (sectionItemIndex < 0 || sectionItemIndex == NSNotFound ||
        cellItemIndex < 0 || cellItemIndex == NSNotFound) {
        return;
    }
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:cellItemIndex inSection:sectionItemIndex];
    [self.collectionView scrollToItemAtIndexPath:indexPath atScrollPosition:position animated:animated];
}

- (void)scrollToTopAnimated:(BOOL)animated {
    UIEdgeInsets insets = self.collectionView.contentInset;
    [self.collectionView setContentOffset:CGPointMake(0, insets.top) animated:animated];
}

#pragma mark - Helpers

- (id<RSBCollectionViewCellItemProtocol>)cellItemByIndexPath:(NSIndexPath *)indexPath {
    id<RSBCollectionViewSectionItemProtocol> sectionItem = [self sectionItemByIndex:indexPath.section];
    if (indexPath.row < sectionItem.cellItems.count) {
        return sectionItem.cellItems[indexPath.item];
    }
    return nil;
}

- (id<RSBCollectionViewSectionItemProtocol>)sectionItemByIndex:(NSUInteger)index {
    if (index < self.sectionItems.count) {
        return self.sectionItems[index];
    }
    return nil;
}

- (void)registerSectionItem:(id<RSBCollectionViewSectionItemProtocol>)sectionItem{
    NSAssert([[sectionItem class] conformsToProtocol:@protocol(RSBCollectionViewSectionItemProtocol)], @"Section items should conform to CollectionViewSectionItemProtocol");
    [sectionItem.cellItems enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        NSAssert([[obj class] conformsToProtocol:@protocol(RSBCollectionViewCellItemProtocol)], @"Cell items should conform to RSBCollectionViewCellItemProtocol");
        id<RSBCollectionViewCellItemProtocol> cellItem = obj;
        [self registerCellItem:cellItem];
    }];
    [sectionItem.reusableViewItems enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
        id<RSBCollectionViewReusableViewItemProtocol> reusableViewItem = obj;
        [reusableViewItem.class registerReusableViewForCollectionView:self.collectionView kind:key];
    }];
}

- (void)registerCellItem:(id<RSBCollectionViewCellItemProtocol>)cellItem {
    NSString *reuseIdentifier = NSStringFromClass(cellItem.class);
    NSString *possibleStoryboardReuseIdentifier = [self reuseIdentifierForCellItem:cellItem];
    
    BOOL usedStoryboardReuseIdentifier = ![reuseIdentifier isEqualToString:possibleStoryboardReuseIdentifier];
    
    if (!usedStoryboardReuseIdentifier) {
        SEL nibSelector = @selector(registeredCollectionViewCellNib);
        SEL classSelector = @selector(registeredCollectionViewCellClass);
        
        if ([cellItem respondsToSelector:nibSelector]) {
            UINib *nib = cellItem.registeredCollectionViewCellNib;
            NSAssert(nib != nil, @"Your cell item %@ adopt %@ method, but provide nil", reuseIdentifier, NSStringFromSelector(nibSelector));
            
            [self.collectionView registerNib:nib forCellWithReuseIdentifier:reuseIdentifier];
        } else if ([cellItem respondsToSelector:classSelector]) {
            Class klass = cellItem.registeredCollectionViewCellClass;
            NSAssert(klass != nil, @"Your cell item %@ adopt %@ method, but provide nil", reuseIdentifier, NSStringFromSelector(classSelector));
            
            [self.collectionView registerClass:klass forCellWithReuseIdentifier:reuseIdentifier];
        } else {
            [NSException raise:@"RSBCollectionViewManager register exception" format:@"You have to provide at least one of the following methods registeredCollectionViewCellNib, registeredCollectionViewCellClass or storyboardPrototypeCollectionViewCellReuseIdentifier."];
        }
    }
}

- (NSString *)reuseIdentifierForCellItem:(id<RSBCollectionViewCellItemProtocol>)cellItem {
    NSString *reuseIdentifier = NSStringFromClass(cellItem.class);
    
    SEL storyboardSelector = @selector(storyboardPrototypeCollectionViewCellReuseIdentifier);
    if ([cellItem respondsToSelector:storyboardSelector]) {
        NSString *storyboardReuseIdentifier = cellItem.storyboardPrototypeCollectionViewCellReuseIdentifier;
        NSAssert(storyboardReuseIdentifier != nil, @"Your cell item %@ adopt %@ method, but provide nil", reuseIdentifier, NSStringFromSelector(storyboardSelector));
        
        reuseIdentifier = storyboardReuseIdentifier;
    }
    
    return reuseIdentifier;
}

#pragma mark - UICollectionViewDatasource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return self.sectionItems.count;
}

- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section{
    id<RSBCollectionViewSectionItemProtocol> sectionItem = [self sectionItemByIndex:section];
    return sectionItem.cellItems.count;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView
           viewForSupplementaryElementOfKind:(NSString *)kind
                                 atIndexPath:(NSIndexPath *)indexPath{
    id<RSBCollectionViewSectionItemProtocol> sectionItem = [self sectionItemByIndex:indexPath.section];
    id<RSBCollectionViewReusableViewItemProtocol> reusableViewItem = sectionItem.reusableViewItems[kind];
    return [reusableViewItem reusableViewForCollectionView:collectionView kind:kind indexPath:indexPath];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    id<RSBCollectionViewCellItemProtocol> cellItem = [self cellItemByIndexPath:indexPath];
    UICollectionViewCell *cell = [self.collectionView dequeueReusableCellWithReuseIdentifier:[self reuseIdentifierForCellItem:cellItem] forIndexPath:indexPath];
    [cellItem configureCell:cell atIndexPath:indexPath inCollectionView:cv];
    return cell;
}

#pragma mark - UICollectionViewDelegate

- (BOOL)collectionView:(UICollectionView *)collectionView shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath {
    id<RSBCollectionViewCellItemProtocol> cellItem = [self cellItemByIndexPath:indexPath];
    return [cellItem itemShouldHighlightInCollectionView:collectionView atIndexPath:indexPath];
}

- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    id<RSBCollectionViewCellItemProtocol> cellItem = [self cellItemByIndexPath:indexPath];
    return [cellItem itemShouldSelectInCollectionView:collectionView atIndexPath:indexPath];
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    id<RSBCollectionViewCellItemProtocol> cellItem = [self cellItemByIndexPath:indexPath];
    [cellItem itemDidSelectInCollectionView:collectionView atIndexPath:indexPath];
}

- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath{
    id<RSBCollectionViewCellItemProtocol> cellItem = [self cellItemByIndexPath:indexPath];
    [cellItem itemDidDeselectInCollectionView:collectionView atIndexPath:indexPath];
}

- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath {
    id<RSBCollectionViewCellItemProtocol> cellItem = [self cellItemByIndexPath:indexPath];
    if ([cellItem respondsToSelector:@selector(willDisplayCell:forItemAtIndexPath:inCollectionView:)]) {
        [cellItem willDisplayCell:cell forItemAtIndexPath:indexPath inCollectionView:collectionView];
    }
}

- (void)collectionView:(UICollectionView *)collectionView didEndDisplayingCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath {
    id<RSBCollectionViewCellItemProtocol> cellItem = [self cellItemByIndexPath:indexPath];
    if ([cellItem respondsToSelector:@selector(didEndDisplayingCell:forItemAtIndexPath:inCollectionView:)]) {
        [cellItem didEndDisplayingCell:cell forItemAtIndexPath:indexPath inCollectionView:collectionView];
    }
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    id<RSBCollectionViewCellItemProtocol> cellItem = [self cellItemByIndexPath:indexPath];
    return [cellItem sizeForCollectionView:collectionView layout:collectionViewLayout indexPath:indexPath];
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    id<RSBCollectionViewSectionItemProtocol> sectionItem = [self sectionItemByIndex:section];
    return [sectionItem insetForCollectionView:collectionView layout:collectionViewLayout sectionIndex:section];
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    id<RSBCollectionViewSectionItemProtocol> sectionItem = [self sectionItemByIndex:section];
    return [sectionItem minimumLineSpacingForCollectionView:collectionView layout:collectionViewLayout sectionIndex:section];
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    id<RSBCollectionViewSectionItemProtocol> sectionItem = [self sectionItemByIndex:section];
    return [sectionItem minimumInteritemSpacingForCollectionView:collectionView layout:collectionViewLayout sectionIndex:section];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    id<RSBCollectionViewSectionItemProtocol> sectionItem = [self sectionItemByIndex:section];
    id<RSBCollectionViewReusableViewItemProtocol> reusableViewItem = sectionItem.reusableViewItems[UICollectionElementKindSectionHeader];
    return [reusableViewItem sizeForCollectionView:collectionView layout:collectionViewLayout section:section kind:UICollectionElementKindSectionHeader];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section{
    id<RSBCollectionViewSectionItemProtocol> sectionItem = [self sectionItemByIndex:section];
    id<RSBCollectionViewReusableViewItemProtocol> reusableViewItem = sectionItem.reusableViewItems[UICollectionElementKindSectionFooter];
    return [reusableViewItem sizeForCollectionView:collectionView layout:collectionViewLayout section:section kind:UICollectionElementKindSectionFooter];
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if ([self.scrollDelegate respondsToSelector:@selector(scrollViewDidScroll:)]){
        [self.scrollDelegate scrollViewDidScroll:scrollView];
    }
}

- (void)scrollViewDidZoom:(UIScrollView *)scrollView{
    if ([self.scrollDelegate respondsToSelector:@selector(scrollViewDidZoom:)]){
        [self.scrollDelegate scrollViewDidZoom:scrollView];
    }
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    if ([self.scrollDelegate respondsToSelector:@selector(scrollViewWillBeginDragging:)]){
        [self.scrollDelegate scrollViewWillBeginDragging:scrollView];
    }
}

- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView
                     withVelocity:(CGPoint)velocity
              targetContentOffset:(inout CGPoint *)targetContentOffset{
    if ([self.scrollDelegate respondsToSelector:@selector(scrollViewWillEndDragging:withVelocity:targetContentOffset:)]){
        [self.scrollDelegate scrollViewWillEndDragging:scrollView withVelocity:velocity targetContentOffset:targetContentOffset];
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    if ([self.scrollDelegate respondsToSelector:@selector(scrollViewDidEndDragging:willDecelerate:)]){
        [self.scrollDelegate scrollViewDidEndDragging:scrollView willDecelerate:decelerate];
    }
}

- (void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView{
    if ([self.scrollDelegate respondsToSelector:@selector(scrollViewWillBeginDecelerating:)]){
        [self.scrollDelegate scrollViewWillBeginDecelerating:scrollView];
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    if ([self.scrollDelegate respondsToSelector:@selector(scrollViewDidEndDecelerating:)]){
        [self.scrollDelegate scrollViewDidEndDecelerating:scrollView];
    }
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView{
    if ([self.scrollDelegate respondsToSelector:@selector(scrollViewDidEndScrollingAnimation:)]){
        [self.scrollDelegate scrollViewDidEndScrollingAnimation:scrollView];
    }
}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView{
    if ([self.scrollDelegate respondsToSelector:@selector(viewForZoomingInScrollView:)]){
        return [self.scrollDelegate viewForZoomingInScrollView:scrollView];
    }
    return nil;
}

- (void)scrollViewWillBeginZooming:(UIScrollView *)scrollView withView:(UIView *)view{
    if ([self.scrollDelegate respondsToSelector:@selector(scrollViewWillBeginZooming:withView:)]){
        [self.scrollDelegate scrollViewWillBeginZooming:scrollView withView:view];
    }
}

- (void)scrollViewDidEndZooming:(UIScrollView *)scrollView withView:(UIView *)view atScale:(CGFloat)scale{
    if ([self.scrollDelegate respondsToSelector:@selector(scrollViewDidEndZooming:withView:atScale:)]){
        [self.scrollDelegate scrollViewDidEndZooming:scrollView withView:view atScale:scale];
    }
}

- (BOOL)scrollViewShouldScrollToTop:(UIScrollView *)scrollView{
    if ([self.scrollDelegate respondsToSelector:@selector(scrollViewShouldScrollToTop:)]){
        return [self.scrollDelegate scrollViewShouldScrollToTop:scrollView];
    }
    return YES;
}

- (void)scrollViewDidScrollToTop:(UIScrollView *)scrollView{
    if ([self.scrollDelegate respondsToSelector:@selector(scrollViewDidScrollToTop:)]){
        [self.scrollDelegate scrollViewDidScrollToTop:scrollView];
    }
}

@end
