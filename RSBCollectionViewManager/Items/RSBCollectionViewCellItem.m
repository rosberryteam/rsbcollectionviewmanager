//Copyright (c) 2015 RSBCollectionViewManager
//
//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in
//all copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//THE SOFTWARE.

#import "RSBCollectionViewCellItem.h"

@implementation RSBCollectionViewCellItem

- (CGSize)sizeForCollectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout indexPath:(NSIndexPath *)indexPath {
    [NSException raise:[NSString stringWithFormat:@"%@ exception", NSStringFromClass([self class])] format:@"This method should be overriden by subclass"];
    return CGSizeZero;
}

- (void)itemDidSelectInCollectionView:(UICollectionView *)collectionView atIndexPath:(NSIndexPath *)indexPath {
    if (self.itemDidSelectBlock) {
        self.itemDidSelectBlock(collectionView, indexPath);
    }
}

- (void)itemDidDeselectInCollectionView:(UICollectionView *)collectionView atIndexPath:(NSIndexPath *)indexPath {
    if (self.itemDidDeselectBlock) {
        self.itemDidDeselectBlock(collectionView, indexPath);
    }
}

- (BOOL)itemShouldSelectInCollectionView:(UICollectionView *)collectionView atIndexPath:(NSIndexPath *)indexPath {
    if (self.itemShouldSelectBlock) {
        return self.itemShouldSelectBlock(collectionView, indexPath);
    }
    return YES;
}

- (BOOL)itemShouldHighlightInCollectionView:(UICollectionView *)collectionView atIndexPath:(NSIndexPath *)indexPath {
    if (self.itemShouldHighlightBlock) {
        return self.itemShouldHighlightBlock(collectionView, indexPath);
    }
    return YES;
}

- (void)configureCell:(UICollectionViewCell *)cell atIndexPath:(NSIndexPath *)indexPath inCollectionView:(UICollectionView *)collectionView {
    
}

@end
