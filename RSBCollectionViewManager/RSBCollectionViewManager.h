//Copyright (c) 2015 RSBCollectionViewManager
//
//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in
//all copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//THE SOFTWARE.

#import <UIKit/UIKit.h>

#import "RSBCollectionViewCellItem.h"
#import "RSBCollectionViewSectionItem.h"
#import "RSBCollectionReusableViewItem.h"

NS_ASSUME_NONNULL_BEGIN

@protocol RSBCollectionViewSectionItemProtocol;
@protocol RSBCollectionViewCellItemProtocol;

typedef void(^RSBCollectionViewManagerCompetionBlock)(BOOL finished);

/*!
 * @discussion This manager encapsulate all UICollectionView stuff and allow you to work only
 * with section items - objects that adopt RSBCollectionViewSectionItemProtocol.
 * @see RSBCollectionViewSectionItemProtocol.
 */
@interface RSBCollectionViewManager : NSObject

///Collection view currently handled by manager.
@property (weak, nonatomic, readonly) UICollectionView *collectionView;
///If you need to be notified about scroll events in collection view - you are welcome.
@property (nonatomic, weak, nullable) id<UIScrollViewDelegate> scrollDelegate;
///Current section items.
@property (nonatomic) NSArray *sectionItems;

///Use this method for creating manager.
- (instancetype)initWithCollectionView:(UICollectionView*)collectionView NS_DESIGNATED_INITIALIZER;

/*!
 * @discussion Set section items
 * @param completion If specified collection view will call performBatchUpdates:completion: method, instead of simple reloadData if not. 
 */
- (void)setSectionItems:(NSArray *)sectionItems
             completion:(nullable RSBCollectionViewManagerCompetionBlock)completion;

/*!
 * @discussion Remove cell items from concrete section item.
 * @param cellItems Array of cell items you want to remove.
 * @param sectionItem Section item from which you want to remove items.
 * @param completion A completion handler block to execute when all of the operations are finished. This block takes a single Boolean parameter that contains the value YES if all of the related animations completed successfully or NO if they were interrupted. This parameter may be nil.
 */
- (void)removeCellItems:(NSArray <id<RSBCollectionViewCellItemProtocol>> *)cellItems
        fromSectionItem:(id<RSBCollectionViewSectionItemProtocol>)sectionItem
             completion:(nullable RSBCollectionViewManagerCompetionBlock)completion;
/*!
 * @discussion Insert cell items to concrete section item.
 * @param cellItems Array of cell items you want to insert.
 * @param sectionItem Section item to which you want to insert items.
 * @param indexes Index set of cell item's indexes you want to insert to.
 * @param completion A completion handler block to execute when all of the operations are finished. This block takes a single Boolean parameter that contains the value YES if all of the related animations completed successfully or NO if they were interrupted. This parameter may be nil.
 */
- (void)insertCellItems:(NSArray <id<RSBCollectionViewCellItemProtocol>> *)cellItems
          toSectionItem:(id<RSBCollectionViewSectionItemProtocol>)sectionItem
              atIndexes:(NSIndexSet *)indexes
             completion:(nullable RSBCollectionViewManagerCompetionBlock)completion;
/*!
 * @discussion Repace cell items in concrete section item.
 * @param indexes Index set of cell item's indexes you want to replace.
 * @param cellItems Array of cell items you want to insert.
 * @param sectionItem Section item to which you want to insert items.
 * @param completion A completion handler block to execute when all of the operations are finished. This block takes a single Boolean parameter that contains the value YES if all of the related animations completed successfully or NO if they were interrupted. This parameter may be nil.
 */
- (void)replaceCellItemsAtIndexes:(NSIndexSet *)indexes
                    withCellItems:(NSArray<id<RSBCollectionViewCellItemProtocol>> *)cellItems
                    inSectionItem:(id<RSBCollectionViewSectionItemProtocol>)sectionItem
                       completion:(nullable RSBCollectionViewManagerCompetionBlock)completion;

/*!
 * @discussion Remove cell items from concrete section item.
 * @param sectionItems Array of section items you want to remove.
 * @param completion A completion handler block to execute when all of the operations are finished. This block takes a single Boolean parameter that contains the value YES if all of the related animations completed successfully or NO if they were interrupted. This parameter may be nil.
 */
- (void)removeSectionItems:(NSArray<id<RSBCollectionViewSectionItemProtocol>> *)sectionItems
                completion:(nullable RSBCollectionViewManagerCompetionBlock)completion;
/*!
 * @discussion Insert cell items to concrete section item.
 * @param sectionItems Array of section items you want to insert.
 * @param indexes Index set of section item's indexes you want to insert to.
 * @param completion A completion handler block to execute when all of the operations are finished. This block takes a single Boolean parameter that contains the value YES if all of the related animations completed successfully or NO if they were interrupted. This parameter may be nil.
 */
- (void)insertSectionItems:(NSArray<id<RSBCollectionViewSectionItemProtocol>> *)sectionItems
                 atIndexes:(NSIndexSet *)indexes
                completion:(nullable RSBCollectionViewManagerCompetionBlock)completion;
/*!
 * @discussion Replace section items.
 * @param indexes Index set of section items you want to replace.
 * @param array Array of section items you want to insert.
 * @param completion A completion handler block to execute when all of the operations are finished. This block takes a single Boolean parameter that contains the value YES if all of the related animations completed successfully or NO if they were interrupted. This parameter may be nil.
 */
- (void)replaceSectionItemsAtIndexes:(NSIndexSet *)indexes
                    withSectionItems:(NSArray<id<RSBCollectionViewSectionItemProtocol>> *)array
                          completion:(nullable RSBCollectionViewManagerCompetionBlock)completion;

///You can get frame of concrete cell item cell.
- (CGRect)frameForCellItem:(id<RSBCollectionViewCellItemProtocol>)cellItem inSectionItem:(id<RSBCollectionViewSectionItemProtocol>)sectionItem;
///Scroll to top.
- (void)scrollToCellItem:(id<RSBCollectionViewCellItemProtocol>)cellItem
           inSectionItem:(id<RSBCollectionViewSectionItemProtocol>)sectionItem
        atScrollPosition:(UICollectionViewScrollPosition)position
                animated:(BOOL)animated;
///Scroll to top.
- (void)scrollToTopAnimated:(BOOL)animated;

@end

NS_ASSUME_NONNULL_END
