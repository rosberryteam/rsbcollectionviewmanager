//
//  NameCellItem.h
//  CollectionViewManager
//
//  Created by Anton Kovalev on 15.04.16.
//  Copyright © 2016 Rosberry. All rights reserved.
//

#import "RSBCollectionViewCellItem.h"

@interface NameCellItem : RSBCollectionViewCellItem

@property (nonatomic) NSString *name;

@end
