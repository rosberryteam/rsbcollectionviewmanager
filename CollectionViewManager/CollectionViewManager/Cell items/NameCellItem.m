//
//  NameCellItem.m
//  CollectionViewManager
//
//  Created by Anton Kovalev on 15.04.16.
//  Copyright © 2016 Rosberry. All rights reserved.
//

#import "NameCellItem.h"
#import "TextCollectionViewCell.h"

@implementation NameCellItem

- (CGSize)sizeForCollectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout indexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(CGRectGetWidth(collectionView.bounds)/2.0, 50.0);
}

- (void)configureCell:(TextCollectionViewCell *)cell atIndexPath:(NSIndexPath *)indexPath inCollectionView:(UICollectionView *)collectionView {
    [cell.titleLabel setText:self.name];
}

- (UINib *)registeredCollectionViewCellNib {
    return [UINib nibWithNibName:NSStringFromClass([TextCollectionViewCell class]) bundle:nil];
}

@end
