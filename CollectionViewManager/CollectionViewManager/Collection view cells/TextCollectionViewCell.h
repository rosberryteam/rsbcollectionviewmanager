//
//  TextCollectionViewCell.h
//  CollectionViewManager
//
//  Created by Anton Kovalev on 18.04.16.
//  Copyright © 2016 Rosberry. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TextCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end
