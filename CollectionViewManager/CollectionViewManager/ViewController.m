//
//  ViewController.m
//  CollectionViewManager
//
//  Created by Anton Kovalev on 15.04.16.
//  Copyright © 2016 Rosberry. All rights reserved.
//

#import "ViewController.h"
#import "RSBCollectionViewManager.h"
#import "NameCellItem.h"

@interface ViewController ()

@property (nonatomic) IBOutlet UICollectionView *collectionView;
@property (nonatomic) RSBCollectionViewManager *collectionViewManager;
@property (nonatomic) NSArray *names;
@property (nonatomic) NSArray *otherNames;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self configureManagerWithCollectionView:self.collectionView];
    [self createNamesArray];
    [self updateSectionItems];
}

#pragma mark - Configure

- (void)configureManagerWithCollectionView:(UICollectionView *)collectionView {
    self.collectionViewManager = [[RSBCollectionViewManager alloc] initWithCollectionView:collectionView];
}

- (void)createNamesArray {
    self.names = @[@"John", @"Max", @"Andrew", @"Jim", @"Jack", @"Arnold", @"Ryan"];
    self.otherNames = @[@"James", @"Bill", @"Steve", @"Ronald", @"Jason", @"Cameron", @"Alan"];
}

#pragma mark - Cell items

- (NameCellItem *)nameCellItemWithName:(NSString *)name {
    NameCellItem *item = [[NameCellItem alloc] init];
    [item setName:name];
    [item setItemDidSelectBlock:^(UICollectionView *__weak collectionView, NSIndexPath *indexPath) {
//        [self removeAllSectionsButFirst];
//        [self replaceFirstAndSecondSectionItems];
//        [self insertTwoSectionItemsAfterFirstSection];

//        [self removeAllButFirstCellItemsInSectionWithIndex:indexPath.section];
//        [self replaceAllCellItemsInSectionWithIndex:indexPath.section];
//        [self insertNewItemsInSectionWithIndex:indexPath.section];

//        [self scrollToItemAtIndexPath:indexPath];
        [self logFrameForItemAtIndexPath:indexPath];
//        [self scrollToTop];
    }];
    
    [item setItemDidDeselectBlock:^(UICollectionView *__weak collectionView, NSIndexPath *indexPath) {
        NSLog(@"Deselect: %@", indexPath);
    }];
    
    return item;
}

#pragma mark - Section items

- (void)updateSectionItems {
    NSMutableArray *ma = [NSMutableArray array];
    for (int i = 0; i < 5; ++i) {
        RSBCollectionViewSectionItem *sectionItem = [self namesSectionItem];
        [ma addObject:sectionItem];
    }
    
    [self.collectionViewManager setSectionItems:[NSArray arrayWithArray:ma] completion:^(BOOL finished) {
        
    }];
}

- (RSBCollectionViewSectionItem *)namesSectionItem {
    NSMutableArray *ma = [NSMutableArray array];
    for (NSString *name in self.names) {
        [ma addObject:[self nameCellItemWithName:name]];
    }
    
    return [[RSBCollectionViewSectionItem alloc] initWithCellItems:[NSArray arrayWithArray:ma]];
}

- (RSBCollectionViewSectionItem *)otherNamesSectionItem {
    NSMutableArray *ma = [NSMutableArray array];
    for (NSString *name in self.otherNames) {
        [ma addObject:[self nameCellItemWithName:name]];
    }
    
    return [[RSBCollectionViewSectionItem alloc] initWithCellItems:[NSArray arrayWithArray:ma]];
}

#pragma mark - Work with collection
#pragma mark Sections

- (void)removeAllSectionsButFirst {
    NSArray *sectionItems = self.collectionViewManager.sectionItems;
    
    RSBCollectionViewSectionItem *firstItem = sectionItems.firstObject;
    
    NSMutableArray *ma = [NSMutableArray arrayWithArray:sectionItems];
    [ma removeObject:firstItem];
    
    [self.collectionViewManager removeSectionItems:[NSArray arrayWithArray:ma] completion:nil];
}

- (void)replaceFirstAndSecondSectionItems {
    NSIndexSet *indexes = [NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0, 2)];
    
    NSArray *nItems = @[self.otherNamesSectionItem, self.otherNamesSectionItem];
    
    [self.collectionViewManager replaceSectionItemsAtIndexes:indexes withSectionItems:nItems completion:nil];
}

- (void)insertTwoSectionItemsAfterFirstSection {
    NSIndexSet *indexes = [NSIndexSet indexSetWithIndexesInRange:NSMakeRange(1, 2)];
    
    NSArray *nItems = @[self.otherNamesSectionItem, self.otherNamesSectionItem];
    
    [self.collectionViewManager insertSectionItems:nItems atIndexes:indexes completion:nil];
}

#pragma mark Cells

- (void)removeAllButFirstCellItemsInSectionWithIndex:(NSInteger)sectionIndex {
    RSBCollectionViewSectionItem *sectionItem = self.collectionViewManager.sectionItems[sectionIndex];
    
    RSBCollectionViewCellItem *firstItem = sectionItem.cellItems.firstObject;
    
    NSMutableArray *ma = [NSMutableArray arrayWithArray:sectionItem.cellItems];
    [ma removeObject:firstItem];
    
    [self.collectionViewManager removeCellItems:[NSArray arrayWithArray:ma] fromSectionItem:sectionItem completion:nil];
}

- (void)replaceAllCellItemsInSectionWithIndex:(NSInteger)sectionIndex {
    RSBCollectionViewSectionItem *sectionItem = self.collectionViewManager.sectionItems[sectionIndex];
    RSBCollectionViewSectionItem *otherNamesItem = self.otherNamesSectionItem;
    
    NSAssert(sectionItem.cellItems.count == otherNamesItem.cellItems.count, @"Count of items should be equal");
    
    NSIndexSet *indexes = [NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0, sectionItem.cellItems.count)];
    
    [self.collectionViewManager replaceCellItemsAtIndexes:indexes withCellItems:otherNamesItem.cellItems inSectionItem:sectionItem completion:nil];
}

- (void)insertNewItemsInSectionWithIndex:(NSInteger)sectionIndex {
    RSBCollectionViewSectionItem *sectionItem = self.collectionViewManager.sectionItems[sectionIndex];
    RSBCollectionViewSectionItem *otherNamesItem = self.otherNamesSectionItem;
    
    NSIndexSet *indexes = [NSIndexSet indexSetWithIndexesInRange:NSMakeRange(sectionItem.cellItems.count, otherNamesItem.cellItems.count)];
    
    [self.collectionViewManager insertCellItems:otherNamesItem.cellItems toSectionItem:sectionItem atIndexes:indexes completion:nil];
}

#pragma mark - Other manager methods

- (void)scrollToItemAtIndexPath:(NSIndexPath *)indexPath {
    RSBCollectionViewSectionItem *sectionItem = self.collectionViewManager.sectionItems[indexPath.section];
    RSBCollectionViewCellItem *cellItem = sectionItem.cellItems[indexPath.row];
    
    [self.collectionViewManager scrollToCellItem:cellItem inSectionItem:sectionItem atScrollPosition:UICollectionViewScrollPositionTop animated:YES];
}

- (void)logFrameForItemAtIndexPath:(NSIndexPath *)indexPath {
    RSBCollectionViewSectionItem *sectionItem = self.collectionViewManager.sectionItems[indexPath.section];
    RSBCollectionViewCellItem *cellItem = sectionItem.cellItems[indexPath.row];
    
    CGRect frame = [self.collectionViewManager frameForCellItem:cellItem inSectionItem:sectionItem];
    
    NSLog(@"[Frame] [Section %li, Row %li] %@", (long)indexPath.section, (long)indexPath.row, NSStringFromCGRect(frame));
}

- (void)scrollToTop {
    [self.collectionViewManager scrollToTopAnimated:YES];
}

@end
