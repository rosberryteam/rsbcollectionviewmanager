# RSBCollectionViewManager #

* This manager encapsulate all `UICollectionView` stuff and allow you to work only with section items - objects that adopt `RSBCollectionViewSectionItemProtocol` and represent sections of `UICollectionView`. This section items contains cell items - objects that adopt `RSBCollectionViewCellItemProtocol` and represent cells of `UICollectionView`. You will never need to write any `UICollectionViewDelegate` or `UICollectionViewDataSource` methods in your `UIViewController` class.
* Version 0.0.2

### Install via Cocoapods ###

You need to add source first.
```
source 'https://bitbucket.org/rosberryteam/cocoapodsspecs.git'
```
Then add
```
pod 'RSBCollectionViewManager'
```
to your `Podfile` and run
```
pod install
```

### How to use ###
Download repo and run example project.

RSBCollectionViewManager works with section items, which contains cell items. So you need to do next steps for beginning:

1. Subclass `RSBCollectionViewSectionItem` or create/use your own class which will adopt `RSBCollectionViewSectionItemProtocol` for section items
2. Subclass `RSBCollectionViewCellItem` or create/use your own class which will adopt `RSBCollectionViewCellItemProtocol` for cell items.

Each cell item represents one cell class at time and contains all related datasource and delegate methods for right displaying it. 

When you will finish with setting up your section and (or) cell items you will need to create instance of `RSBCollectionViewManager` with your `UICollectionView` and set your section items to property `sectionItems` and thats it.